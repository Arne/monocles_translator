<!-- Use this form if you need help (e.g. with using feature X) -->
### What is your problem?
Please describe precisely.

<!-- Depending on the question helpful -->
### Environment data
- Android version:

- Device model: 

- Stock or customized system:

- monocles translator app version:

### Relevant  screenshots
- Paste any relevant screenshots

/label ~support 
