## Introduction
This privacy policy covers the use of the 'monocles translator' (https://codeberg.org/Arne/monocles_translator) Android application.

It may not be applicable to other software developed or released by Arne (https://codeberg.org/Arne).

monocles translator was built as a free and open source application. Is provided free of charge and is intended for use as is.

## Data we collect
monocles translator when running does not collect any statistics, personal information or analytics from its users, other than Android operating system built in mechanisms that are present for all mobile applications.

monocles translator does not contain any advertising sdk, nor tracker of the user or his device.

### History
monocles translator can, at the explicit request of the user, keep a history of translations retrieved from different translation services. This history is stored locally without ever being shared, whether with a third-party service or with developers.

### Cookies
Cookies are not stored at any point. api key and/or authentication credentials can be stored optionally on the user's local device upon the user's explicit request. Developer does not have access to any such information.


## Third party cloud service dependencies
**Data collected by others**

Use of monocles translator may result in one or more of the following third party organisations may collecting data from you. Please check their privacy policy for more information:

### Application Store
* F-droid, store where the app is published - https://f-droid.org/en/about/
<!-- * Google PlayStore, store where the app is published - https://policies.google.com/privacy -->
* Codeberg, Website where the app is published - https://codeberg.org/Codeberg/org/raw/branch/main/PrivacyPolicy.md

### Translation API
**from whom we retrieve the translation data via their API**

* LibreTranslate, open source - https://github.com/LibreTranslate/LibreTranslate
_You can choose the LibreTranslate instance according to your preference._
* Lingva Translate, open source - https://github.com/thedaviddelta/lingva-translate
_You can choose the Lingva Translate instance according to your preference._
* Deepl, closed source - https://www.deepl.com/privacy/
* MyMemory, closed source - https://mymemory.translated.net/doc/en/tos.php

## Android permissions requested by the application
Note that 'monocles translator' application requires android platform permissions:
`android.permission.INTERNET` - In order to perform requests to online translation engines.
`android.permission.ACCESS_NETWORK_STATE` - In order to view information about network connections _(such as which networks exist and are connected)_.
`android.permission.RECORD_AUDIO` - In order to perform a vocal translation, **only at the user's request**.

