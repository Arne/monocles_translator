<!-- ---------- Header ---------- -->
<div align="center">
  <img width="200" height="200"src="app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png">
  <h1>monocles translator</h1>
<p>Powerful Translator App built with <a href="https://m3.material.io/">Material Design 3 (You)</a> supporting different translation engines.</p>

<!-- ---------- Badges ---------- -->
  <div align="center">
    <img alt="License" src="https://img.shields.io/github/license/Bnyro/TranslateYou?color=c3e7ff&style=flat-square">
    <img alt="Downloads" src="https://img.shields.io/github/downloads/Bnyro/TranslateYou/total.svg?color=c3e7ff&style=flat-square">
    <img alt="Last commit" src="https://img.shields.io/github/last-commit/Bnyro/TranslateYou?color=c3e7ff&style=flat-square">
    <img alt="Repo size" src="https://img.shields.io/github/repo-size/Bnyro/TranslateYou?color=c3e7ff&style=flat-square">
    <img alt="Stars" src="https://img.shields.io/github/stars/Bnyro/https://codeberg.org/Arne/monocles_translator/pullsTranslateYou?color=c3e7ff&style=flat-square">
    <br>
</div>
</div>

<!-- ---------- Description ---------- -->
## Features

- [x] more than 150 supported languages
- [x] 9 different translation engines
- [x] Material Design 3 (You)
- [x] Dark and light theme
- [x] Support for Android sharing system
- [x] Multilingual interface
- [x] Translation from images
- [x] Translation history
- [x] Entirely Open Source
- [X] Written in Jetpack Compose 

<!-- ---------- Download ---------- -->
## Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/en/packages/de.monocles.translator/)
[<img src="https://codeberg.org/Codeberg/GetItOnCodeberg/media/branch/main/get-it-on-blue-on-white.png" alt="Get it on Codeberg" height="80">](https://codeberg.org/Arne/monocles_translator/releases)

<!-- ---------- Supported translation engines ---------- -->
## Supported translation engines

* <a href="https://github.com/LibreTranslate/LibreTranslate">LibreTranslate</a>
* <a href="https://github.com/thedaviddelta/lingva-translate">LingvaTranslate</a> _which uses Google Translate® to fetch translations._
* <a href="https://www.deepl.com/translator">DeepL translator®</a>.
* <a href="https://mymemory.translated.net/">MyMemory translator®</a>.
* <a href="https://simple-web.org/projects/simplytranslate.html">SimplyTranslate</a>
* <a href="https://www.reverso.net">Reverso®</a>
* <a href="https://mozhi.aryak.me">Mozhi</a>
* <a href="https://translate.wmcloud.org">MinT</a>
* <a href="https://glosbe.com/">Glosbe</a>
* <a href="https://apertium.org/">Apertium</a>
* <a href="https://github.com/janvarev/OneRingTranslator">OneRingTranslator</a>

<!-- ---------- Screenshots [Plus version] ---------- -->
## Screenshots

<div style="display: flex;">
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width=30%>
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width=30%>
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width=30%>
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width=30%>
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/5.png" width=30%>
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/6.png" width=30%>
</div>

<!-- ---------- Contribution ---------- -->
## Feedback and contributions
***All contributions are very welcome!***

* Feel free to join the [XMPP room](xmpp:support@conference.monocles.de) for discussions about the app.
* Bug reports and feature requests can be submitted [here](https://codeberg.org/Arne/monocles_translator/issues) (please make sure to fill out all the requested information properly!).
* If you are a developer and wish to contribute to the app, please **fork** the project and submit a [**pull request**](https://codeberg.org/Arne/monocles_translator/pulls).

## Translation
<a href="https://hosted.weblate.org/projects/you-apps/translate-you/">
<img src="https://hosted.weblate.org/widgets/you-apps/-/translate-you/287x66-grey.png" alt="Translation status" />
</a>

**Translations are greatly appreciated.** \
If **monocles translator** is not in your language, feel free to participate by:
* Translating the application interface, via [**pull request**](https://help.github.com/articles/about-pull-requests/) or more simply by [**Weblate**](https://hosted.weblate.org/projects/you-apps/translate-you/).
* **Check** and **update** the [**pre-existing translations on Github**](https://github.com/Bnyro/TranslateYou/tree/master/app/src/main/res) or more simply via [**Weblate**](https://hosted.weblate.org/projects/you-apps/translate-you/).

<!-- ---------- Privacy Policy and License ---------- -->
## Privacy Policy

**Translate You's privacy policy is available** [**here**](https://github.com/Bnyro/TranslateYou/blob/master/PRIVACY%20POLICY.md)

## License

monocles translator is licensed under the [**GNU General Public License**](https://www.gnu.org/licenses/gpl.html): You can use, study and share it as you want.
