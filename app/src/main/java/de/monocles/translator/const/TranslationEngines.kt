/*
 * Copyright (c) 2023 You Apps
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.monocles.translator.const

import de.monocles.translator.api.ap.ApEngine
import de.monocles.translator.api.deepl.DeeplEngine
import de.monocles.translator.api.gl.GlEngine
import de.monocles.translator.api.lt.LTEngine
import de.monocles.translator.api.lv.LVEngine
import de.monocles.translator.api.mh.MhEngine
import de.monocles.translator.api.mm.MMEngine
import de.monocles.translator.api.or.OneRingEngine
import de.monocles.translator.api.po.PonsEngine
import de.monocles.translator.api.reverso.ReversoEngine
import de.monocles.translator.api.st.STEngine
import de.monocles.translator.api.wm.WmEngine

object TranslationEngines {
    val engines = listOf(
        LTEngine(),
        LVEngine(),
        DeeplEngine(),
        MMEngine(),
        ReversoEngine(),
        STEngine(),
        MhEngine(),
        WmEngine(),
        GlEngine(),
        ApEngine(),
        OneRingEngine(),
        PonsEngine()
    ).map {
        it.createOrRecreate()
    }

    fun updateAll() = engines.forEach { it.createOrRecreate() }
}
