package de.monocles.translator.const

enum class ThemeMode(val value: Int) {
    AUTO(0),
    LIGHT(1),
    DARK(2),
    BLACK(3)
}
