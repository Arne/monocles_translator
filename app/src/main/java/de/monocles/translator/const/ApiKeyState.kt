package de.monocles.translator.const


enum class ApiKeyState {
    DISABLED,
    OPTIONAL,
    REQUIRED
}