package de.monocles.translator.const

object AboutLinks {
    const val PRIVACY_POLICY = "https://codeberg.org/Arne/monocles_translator/raw/branch/main/PRIVACY_POLICY.md"
    const val AUTHOR = "https://monocles.eu/more"
    const val GITHUB = "https://codeberg.org/monocles/monocles_translator"
    const val GNU = "https://www.gnu.org/licenses/gpl-3.0.en.html"
}
