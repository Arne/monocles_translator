package de.monocles.translator.ext

inline fun <R> expOrNull(block: () -> R): R? {
    return try {
        block()
    } catch (e: Throwable) {
        null
    }
}
