package de.monocles.translator.ext

fun query(dbQuery: () -> Unit) {
    Thread {
        dbQuery.invoke()
    }.start()
}
