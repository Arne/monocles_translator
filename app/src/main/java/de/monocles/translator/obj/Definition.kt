package de.monocles.translator.obj

data class Definition(
    val type: String? = null,
    val definition: String? = null,
    val example: String? = null,
    val synonym: String? = null
)
