package de.monocles.translator.api.wm

import de.monocles.translator.api.wm.obj.WmTranslationRequest
import de.monocles.translator.const.ApiKeyState
import de.monocles.translator.db.obj.Language
import de.monocles.translator.obj.Translation
import de.monocles.translator.util.RetrofitHelper
import de.monocles.translator.util.TranslationEngine
import java.util.*

class WmEngine: TranslationEngine(
    name = "MinT",
    apiKeyState = ApiKeyState.DISABLED,
    supportsSimTranslation = true,
    autoLanguageCode = null,
    defaultUrl = "https://translate.wmcloud.org/",
    urlModifiable = true
) {
    lateinit var api: WikimediaMinT

    override fun createOrRecreate(): TranslationEngine = apply {
        api = RetrofitHelper.createApi(this)
    }

    override suspend fun getLanguages(): List<Language> {
        val json = api.getLanguages()
        return json.entries.map {
            val languageName = runCatching { Locale.forLanguageTag(it.key).displayName }.getOrNull()
            Language(it.key, languageName ?: it.key)
        }
    }

    override suspend fun translate(query: String, source: String, target: String): Translation {
        val response = api.translate(source, target, WmTranslationRequest(query))
        return Translation(response.translation)
    }
}