package de.monocles.translator

import android.app.Application
import de.monocles.translator.db.DatabaseHolder
import de.monocles.translator.util.Preferences
import de.monocles.translator.util.SpeechHelper

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        Preferences.initialize(
            this
        )

        DatabaseHolder().initDb(
            this
        )

        SpeechHelper.initTTS(this)
    }
}
