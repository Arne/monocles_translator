package de.monocles.translator.api.lt.obj

import kotlinx.serialization.Serializable

@Serializable
data class LTDetectedLanguage(
    val language: String? = null
)
