package de.monocles.translator.api.gl

import de.monocles.translator.api.gl.obj.GlTranslationResponse
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface Glosbe {
    @POST("translateByLangDetect")
    suspend fun translate(
            @Query("sourceLang") source: String,
            @Query("targetLang") target: String,
            @Body text: String
    ): GlTranslationResponse
}