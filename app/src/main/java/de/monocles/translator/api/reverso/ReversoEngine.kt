package de.monocles.translator.api.reverso

import de.monocles.translator.api.reverso.obj.ReversoRequestBody
import de.monocles.translator.const.ApiKeyState
import de.monocles.translator.db.obj.Language
import de.monocles.translator.ext.concatenate
import de.monocles.translator.obj.Translation
import de.monocles.translator.util.RetrofitHelper
import de.monocles.translator.util.TranslationEngine

class ReversoEngine : TranslationEngine(
    name = "Reverso",
    defaultUrl = "https://api.reverso.net/",
    urlModifiable = false,
    apiKeyState = ApiKeyState.DISABLED,
    autoLanguageCode = "auto",
    supportsSimTranslation = false
) {
    lateinit var api: Reverso

    override fun createOrRecreate(): TranslationEngine = apply {
        api = RetrofitHelper.createApi(this)
    }

    override suspend fun getLanguages(): List<Language> {
        return listOf(
            Language("ara", "Arabic"),
            Language("chi", "Chinese (Simplified)"),
            Language("dut", "Dutch"),
            Language("eng", "English"),
            Language("fra", "French"),
            Language("ger", "German"),
            Language("heb", "Hebrew"),
            Language("ita", "Italian"),
            Language("jpn", "Japanese"),
            Language("kor", "Korean"),
            Language("pol", "Polish"),
            Language("por", "Portuguese"),
            Language("rum", "Romanian"),
            Language("rus", "Russian"),
            Language("spa", "Spanish"),
            Language("swe", "Swedish"),
            Language("tur", "Turkish"),
            Language("ukr", "Ukrainian")
        )
    }

    override suspend fun translate(query: String, source: String, target: String): Translation {
        val response = api.translate(
            requestBody = ReversoRequestBody(
                from = sourceOrAuto(source),
                to = target,
                input = query
            )
        )

        return Translation(
            translatedText = response.translation.firstOrNull() ?: "",
            detectedLanguage = response.languageDetection?.detectedLanguage,
            similar = response.contextResults?.results
                ?.filter { it.translation != null }
                ?.map { it.translation!! },
            examples = response.contextResults?.results?.let { result ->
                concatenate(
                    result.map { it.sourceExamples }.flatten(),
                    result.map { it.targetExamples }.flatten()
                )
            }
        )
    }
}
