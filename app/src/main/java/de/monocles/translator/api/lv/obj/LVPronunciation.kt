package de.monocles.translator.api.lv.obj

import kotlinx.serialization.Serializable

@Serializable
data class LVPronunciation(
    val query: String? = null
)
