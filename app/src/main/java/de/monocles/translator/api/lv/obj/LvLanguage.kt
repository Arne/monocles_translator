package de.monocles.translator.api.lv.obj

import de.monocles.translator.db.obj.Language
import kotlinx.serialization.Serializable

@Serializable
data class LvLanguage(
    val languages: List<Language> = emptyList()
)
