/*
 * Copyright (c) 2024 You Apps
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.monocles.translator.api.po

import de.monocles.translator.api.po.obj.PonsData
import de.monocles.translator.const.ApiKeyState
import de.monocles.translator.db.obj.Language
import de.monocles.translator.obj.Translation
import de.monocles.translator.util.RetrofitHelper
import de.monocles.translator.util.TranslationEngine
import java.lang.IllegalArgumentException
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

class PonsEngine : TranslationEngine(
    name = "Pons",
    defaultUrl = "https://api.pons.com",
    urlModifiable = false,
    apiKeyState = ApiKeyState.DISABLED,
    autoLanguageCode = ""
) {
    private lateinit var api: Pons

    override fun createOrRecreate(): TranslationEngine = apply {
        api = RetrofitHelper.createApi(this)
    }

    override suspend fun getLanguages(): List<Language> {
        return api.getLanguages()["languages"]?.jsonObject?.map {
            val langName = it.value.jsonObject["display"]?.jsonPrimitive?.content ?: throw IllegalArgumentException()
            Language(it.key, langName)
        }?.sortedBy { it.name } ?: throw IllegalArgumentException()
    }

    override suspend fun translate(query: String, source: String, target: String): Translation {
        val requestBody = PonsData(source.takeIf { it.isNotEmpty() }, target, query)
        val response = api.translate(body = requestBody)

        return Translation(response.text, response.sourceLanguage)
    }
}