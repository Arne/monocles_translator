package de.monocles.translator.api.lt

import de.monocles.translator.const.ApiKeyState
import de.monocles.translator.db.obj.Language
import de.monocles.translator.obj.Translation
import de.monocles.translator.util.RetrofitHelper
import de.monocles.translator.util.TranslationEngine

class LTEngine : TranslationEngine(
    name = "LibreTranslate",
    defaultUrl = "https://translate.monocles.de",
    urlModifiable = true,
    apiKeyState = ApiKeyState.OPTIONAL,
    autoLanguageCode = "auto"
) {

    private lateinit var api: LibreTranslate
    override fun createOrRecreate(): TranslationEngine = apply {
        api = RetrofitHelper.createApi(this)
    }

    override suspend fun getLanguages(): List<Language> {
        return api.getLanguages().map {
            Language(it.code!!, it.name!!)
        }
    }

    override suspend fun translate(
        query: String,
        source: String,
        target: String
    ): Translation {
        val response = api.translate(
            query,
            sourceOrAuto(source),
            target,
            getApiKey()
        )
        return Translation(
            translatedText = response.translatedText,
            detectedLanguage = response.detectedLanguage?.language
        )
    }
}
